# Example Docker .Net Core App
This app gives an example of a docker app structure for use with Visual Studio. 

It will run a .Net Core app that demonstrates how to execute a unix shell process. Therefore, the image installs the _uptime_ package.