﻿using System;
using System.Text.RegularExpressions;

namespace DockerApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            string command = "uptime";
            string result = "";
            string pattern = "(^[0-9:]*)";

            using (System.Diagnostics.Process proc = new System.Diagnostics.Process())
            {
                proc.StartInfo.FileName = "/bin/bash";
                proc.StartInfo.Arguments = "-c \" " + command + " \"";
                proc.StartInfo.UseShellExecute = false;
                proc.StartInfo.RedirectStandardOutput = true;
                proc.StartInfo.RedirectStandardError = true;
                proc.Start();

                result += proc.StandardOutput.ReadToEnd();
                result += proc.StandardError.ReadToEnd();

                proc.WaitForExit();
            }

            Console.WriteLine(result);
//            Console.WriteLine(Regex.Match(result, pattern));
            Console.ReadLine();
        }
    }
}
